(function(root){
  var PT = root.PT = (root.PT || {});

  var Photo = PT.Photo = function(obj) {
    this.obj = _.extend({}, obj);
  }

  Photo._events = {};

  Photo.on = function(eventName, callback){
    if (Photo._events[eventName]){
      Photo._events[eventName].push(callback);
    } else {
      Photo._events[eventName] = [callback];
    }
  }

  Photo.trigger = function(eventName){
    Photo._events[eventName].forEach(function(callback){
      callback();
    });
  }

  Photo.all = [];

  Photo.prototype.get = function(attr) {
    return this.obj[attr];
  }

  Photo.prototype.set = function(attr_name, val) {
    this.obj[attr_name] = val;
  }

  Photo.prototype.create = function(callback) {
    if (this.obj.id) {
      return;
    }

    var that = this;

    $.ajax({
      url: "/api/photos.json",
      type: "POST",
      data: {'photo': that.obj},
      dataType: "json",
      success: function(data){
        callback(data);
        that.obj = _.extend(that.obj, data);
        Photo.all.unshift(that);
        PT.Photo.trigger("add");
      },
      error: function(data) {
        alert("You done fucked up!");
        console.log(that.obj);
        console.log(data);
      }
    });
  }

  Photo.prototype.save = function(callback) {
    if (!this.obj.id) {
      this.create(callback);
      return;
    }

    var that = this;

    $.ajax({
      url: "/api/photos/" + this.obj.id,
      type: "PUT",
      data: {'photo': that.obj},
      dataType: "json",
      success: function(data){
        callback(data);
        that.obj = _.extend(that.obj, data);
      },
      error: function(data) {
        console.log(data);
        alert("gimme backbone or gimme death");
      }
    });
  }

  Photo.fetchByUserId = function(userId, callback){

    $.ajax({
      url: "/api/users/" + userId + "/photos",
      type: "GET",
      success: function(data){
        var photoArr = [];
        data.forEach(function(photo){
          photoArr.push(new Photo(photo));
        });
        PT.Photo.all = photoArr;
        callback(PT.Photo.all);
      }
    });
  }

  var CURRENT_USER_ID;
  $(document).ready(function() {
    CURRENT_USER_ID = +$('#current_user').html();
    PT.initialize();
  })

  _.extend(PT, {
    initialize:   function () {
      PT.Photo.fetchByUserId(CURRENT_USER_ID, function (photosArr) {
        PT.showPhotosIndex(photosArr);
      });
    },

    showPhotoDetail: function (photo) {
      var content = $("#content");

      var photoDetailView = new PT.PhotoDetailView(photo);
      content.html(photoDetailView.render().$el);
    },

    showPhotosIndex: function (photosArr) {
      var $content = $("#content");
      $content.empty();

      var photosListView = new PT.PhotosListView(photosArr);
      $content.append(photosListView.render().$el);

      var photoFormView = new PT.PhotoFormView();
      $content.append(photoFormView.render().$el);
    },

  });

//
//
//
//
// photo tagging below

  var PhotoTagging = PT.PhotoTagging = function(obj) {
    this.obj = _.extend({}, obj);
  }

  PhotoTagging.fetchByPhotoId = function(photoId, callback){

    $.ajax({
      url: "/api/photos/" + photoId + "/photo_taggings",
      type: "GET",
      success: function(data){
        var tagArr = [];
        data.forEach(function(tag){
          tagArr.push(new PhotoTagging(tag));
        });
        PT.PhotoTagging.all = tagArr;
        callback(PT.PhotoTagging.all);
      }
    });
  }

  PhotoTagging._events = {};

  PhotoTagging.on = function(eventName, callback){
    if (PhotoTagging._events[eventName]){
      PhotoTagging._events[eventName].push(callback);
    } else {
      PhotoTagging._events[eventName] = [callback];
    }
  }

  PhotoTagging.trigger = function(eventName){
    PhotoTagging._events[eventName].forEach(function(callback){
      callback();
    });
  }

  PhotoTagging.all = [];

  PhotoTagging.prototype.get = function(attr) {
    return this.obj[attr];
  }

  PhotoTagging.prototype.set = function(attr_name, val) {
    this.obj[attr_name] = val;
  }

  PhotoTagging.prototype.create = function(callback) {
    if (this.obj.id) {
      return;
    }

    var that = this;

    $.ajax({
      url: "/api/photo_taggings.json",
      type: "POST",
      // probably a big ole error
      data: {'photo_tagging': that.obj},
      dataType: "json",
      success: function(data){
        callback(data);
        that.obj = _.extend(that.obj, data);
        PhotoTagging.all.unshift(that);
        // PT.PhotoTagging.trigger("add");
      },
      error: function(data) {
        alert("You done fucked up!");
      }
    });
  }

})(this);