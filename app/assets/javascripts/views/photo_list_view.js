(function(root){
  var PT = root.PT = (root.PT || {});

  var PhotosListView = PT.PhotosListView = function(){
    this.$el = $("<div></div>")
    var that = this;
    PT.Photo.on("add", function() {
      that.render(PT.Photo.all);
    });

    var that = this;

    this.$el.on("click", "li", function(event) {
      event.preventDefault();
      var $li = $(this);
      var id = +$li.attr("data-id");
      var photo = _.find(PT.Photo.all, function(photo) {
        return photo.obj.id == id;
      });
      PT.showPhotoDetail(photo);
    });


  }

  PhotosListView.prototype.render = function(){
    this.$el.empty();
    var $ul = $("<ul></ul>")
    this.$el.append($ul);

    PT.Photo.all.forEach(function(photo){
      $ul.append($("<li data-id=" + photo.obj.id + ">" + "<a href=#>" + photo.obj.title + "</a></li>"));
    });

    return this;
  }

  PhotosListView.prototype.showDetail = function(event, photoObj){
    var photo = new PT.PhotoDetailView(photoObj);
    photo.render();
  }

})(this);