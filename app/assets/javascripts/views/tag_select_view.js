(function(root){
  var PT = root.PT = (root.PT || {});

  var TagSelectView = PT.TagSelectView = function(photo, event){
    this.$el = $("<div></div>")
    this.photo = photo;
    this.event = event;
    var that = this;

    this.$el.on("click", "ul.tag-options li", function(event){
      event.preventDefault();

      var newTag = new PT.PhotoTagging({
        photo_id: that.photo.obj.id,
        user_id: +$(this).attr("data-id"), // or currentTarget
        x_pos: that.offsetX,
        y_pos: that.offsetY
      })

      newTag.create(function(){});
      that.$el.remove();
    })
  }

  TagSelectView.prototype.render = function(){
    var innerDiv = $("<div></div>").addClass("photo-tag");
    this.offsetX = this.event.offsetX;
    this.offsetY = this.event.offsetY;

    innerDiv.css("position", "absolute");
    innerDiv.css("left", this.offsetX - 50);
    innerDiv.css("top", this.offsetY + 100);

    var USERS;

    $(document).ready(function() {
      USERS = JSON.parse($('#all_users').html());
    })

    var users = JST["photo_tag_options"]({users: USERS});

    innerDiv.prepend($(users));

    this.$el.append(innerDiv);

    return this;
  }

})(this);