(function(root){
  var PT = root.PT = (root.PT || {});

  var PhotoFormView = PT.PhotoFormView = function(){
    this.$el = $("<div></div>");
    var that = this;

    this.$el.on("submit", "#photoForm",function(event) {
      event.preventDefault();
      var photoObj = ($(this).serializeJSON().photo);
      that.submit(event, photoObj);
    });
  }

  PhotoFormView.prototype.render = function() {
    var form = JST["photo_form"]();
    this.$el.append($(form));
    return this;
  }

  PhotoFormView.prototype.submit = function(event, photoObj) {
    var photo = new PT.Photo(photoObj);
    photo.create(function() { });
  }
})(this);