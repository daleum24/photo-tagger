(function(root) {
  var PT = root.PT = (root.PT || {});

  var PhotoDetailView = PT.PhotoDetailView  = function(photo){
    this.$el = $("<div></div>");
    this.photo = photo;

    var that = this;
    console.log($("img"));

    this.$el.on("click", "img", function(event) {
      event.preventDefault();
      that.popTagSelectView(event);
    })

  }

  PhotoDetailView.prototype.render = function(){
    var details = JST["photo_detail"]({photo: this.photo});

    this.$el.empty();
    this.$el.append($(details));
    this.$el.append($("<br><a href='/'>Back</a>"));
    return this;
  }

  PhotoDetailView.prototype.popTagSelectView = function(event){
    var tagSV = new PT.TagSelectView(this.photo, event);
    this.$el.append(tagSV.render().$el);
    console.log(this.$el.html());
    return this;
  }

})(this);